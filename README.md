### 简介

该项目ydp3是Yolov8数据处理器2的基础上，修改软件架构，让其更适用于服务器开发，并在功能x5和x8上使用了多线程，使得扩增数据更加高效的一个新项目。作者实测测试可以将数据集扩增速度（x5和x8速度）提升500%以上（16核cpu，win系统）

### 项目概况

本项目进行数据处理可以通过代码和软件两个方式是进行，操作都差不多（**一种是代码运行**，这种方式是多进程；**另一种是直接软件运行**，soft_ware下有两个系统版本win和ubuntu的软件，可以直接使用，**但是是单进程的形式**；如果你希望获得可直接使用的win或者ubuntu系统**多进程**的软件，可以看完本文项目的基本使用过程后，向作者支付20元后，在B站搜索“易派深”，向作者私信dpy3后，作者将会把这个软件以百度网盘或者其他方便的形式发给您）

### 通过代码运行（基本操作）

你可以通过直接运行src文件夹下的main.py来运行该项目（该文件夹下仅main.py和utils.py是运行项目的代码，其余代码都是作者在开发这个项目时的测试代码，方便开发者更好的了解本项目的运行逻辑），如果不出意外，你将会在控制台看到如下界面（直接使用软件也是类似这个界面）：

![1710303528283](image/README/1710303528283.png)

接着你需要进入需要处理的数据集，这个数据集是一个文件夹，文件夹中应该包含img和label（可以有classes.txt），如下：

![1710304824018](image/README/1710304824018.png)

接着让软件进入这个文件夹：

![1710305204866](image/README/1710305204866.png)

没错，你可以通过"cd“命令来进入想要处理的文件夹,接着我们可以通过”x5“功能来让这个数据集进行”x5“扩增：

![1710305231263](image/README/1710305231263.png)

![1710305252695](image/README/1710305252695.png)

在数据集同级目录下将会出现数据集名_x5名称的文件夹，里面放置的就是”x5“后的数据集，接着我们可以进行x8操作，但是在此之前，我们我要将软件进入x5后的文件夹，再进行x8的操作（x5，x8等功能可以单独执行，并不是先执行完成哪个才能后执行哪个，每个功能都是按需使用的）。

因此，此时你可以输入cd C:\Users\ASUS\Desktop\img_x5来进入”x5“后的数据集，但是这里更推荐使用如下操作（先cd ..，后cd img_x5，进入相关文件夹）:

![1710305564776](image/README/1710305564776.png)

![1710305596142](image/README/1710305596142.png)

在img_x5文件夹下将会出现img_x5_x8文件夹，至此，数据扩增40倍结束，但是这个时候，数据集中命名比较混乱并且几乎相同的图片比较集中：

![1710305705884](image/README/1710305705884.png)

因此我们还需要通过"d"功能来打乱数据集（并进行一定程度的重命名），

![1710305795264](image/README/1710305795264.png)

img_x5_x8文件夹下将会出现一个img_x5_x8_d文件夹，里面就是打乱后的结果：

![1710305860663](image/README/1710305860663.png)

这样的结果也可以直接分割数据集后直接使用，但是作者这里更加推荐再重命名一下，因为通常数据集都是0001，0002，0003。。。0999这样的4位数长度的格式，而目前的格式是0，1，2。。。999这样的格式。因此这里再通过“r”功能来重命名数据集（r可以直接使用，默认长度为5，当然如果长度不够用了，你可以用其他的长度，如"r 8"，但是请保证长度值大于等于2，小于等于8）：

![1710306122067](image/README/1710306122067.png)

![1710306268120](image/README/1710306268120.png)

最后可以使用"s"，来划分数据集，可以直接使用"s"命令，默认划分成7:2:1的比例作为数据集划分成训练集:测试集:验证集的比例，或者你也可以使用其他比例，例如"s 8:1:1",将8:1:1划分成训练集:测试集:验证集的比例

划分数据集这里还有两个需要注意的地方。一个是代码根目录或者软件同目录下需要有一个config.json文件，项目需要从这个软件中获得类别，你可以通过功能"m"来进行生成这个文件，

![1710306824473](image/README/1710306824473.png)

如下，你需要将这个文件中的类别按照如下的方式写入：

![1710306912290](image/README/1710306912290.png)

例如你的数据集的classes.txt中长这样：

![1710307009069](image/README/1710307009069.png)

第一个是eyes，第二个是mouse,那么就在config.json中写入：{"names": {"0": "eyes", "1": "mouse"}}

写入完成之后，就可以使用"s"功能进行分割数据集了，但是现在就有第二个需要注意的地方了，通过这样的一堆操作之后，数据集文件名为：img_x5_x8_d_r6，这显然太长了，使用”s“功能后分割数据集后会有一个d.yaml文件，里面的路径会涉及到数据集的命名，如果是”s“功能后再去更改命名或者移动文件夹，会出现找不到文件的概况，因此更改文件夹位置或者更改数据集名称，你应该先确定后，再使用这”s“这个功能。

例如，我将img_x5_x8_d_r6这个文件夹改名为img2后，再进行划分功能：

![1710308132118](image/README/1710308132118.png)

接着在img2同级目录下就会出现img2_s811文件夹，这就是可以直接用于训练的文件夹了，我们可以复制这个文件夹下的yaml文件的地址，放到训练指令中即可，如下：

![1710308951104](image/README/1710308951104.png)

![1710309097811](image/README/1710309097811.png)

![1710309112857](image/README/1710309112857.png)

到这里，基本的使用就完成了，



### 通过软件运行

直接使用软件步骤和通过代码运行是一样的，在soft_ware文件夹下发现有如下两个软件

![1710303614325](image/README/1710303614325.png)

前者是ubuntu下的项目打包软件（单进程），可以在ubuntu系统下通过终端运行，如下：

![1710303874910](image/README/1710303874910.png)

后者是win系统下的项目打包软件（单进程），可以在win系统下通过双击或者也是通过终端运行，如下：

![1710304029432](image/README/1710304029432.png)


### 获得多进程软件（win或者ubuntu）

如果你希望获得可直接使用的win或者ubuntu系统**多进程**的软件，可以看完本文项目的基本使用过程后，向作者支付20元后，在B站搜索“易派深”，向作者私信ydp3后，作者将会把这个软件以百度网盘或者其他方便的形式发给您

![1710309763173](image/README/1710309763173.png)



###简介


###简介

###简介
