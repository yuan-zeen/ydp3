from tqdm import tqdm
import multiprocessing as mp
import time
def doit(t):
    time.sleep(t)
if __name__=='__main__':

    data=range(10)
    pbar = tqdm(total=len(data))
    pbar.set_description('Sleep')
    update = lambda *args: pbar.update()

    n_proc = 5
    pool = mp.Pool(n_proc)
    for d in data:
        pool.apply_async(doit, (d,), callback=update)
    pool.close()
    pool.join()
