import multiprocessing as mp
import time
def test_func(v):
    time.sleep(v)  # v越大 运行时间越少
    print(v)
    return v


if __name__=='__main__':
    # 所需要测试的数据
    data = range(10)
    n_proc = 5
    pool = mp.Pool(n_proc)
    res = []
    start_time=time.time()
    for d in data:
        pool.apply_async(test_func, (d,))
        # tmp = pool.apply_async(test_func, (d,))
        # res.append(tmp)

    pool.close()
    pool.join()
    # for r in res:
    #     r.get() # 返回值需要用get()拿到。
    print(round(time.time()-start_time,1))

