from tqdm import tqdm
import multiprocessing as mp
import time
import multiprocessing as mp
import os
from rich.progress import track
from shutil import copyfile
import json
import yaml
from random import shuffle
from rich.console import Console
from rich.table import Table
import cv2
from copy import deepcopy
from utils import *
console = Console()
import time
def doit(file):
    copyfile("img/"+file, "result/"+file)#先把原来所有文件都复制一份
    if file.split(".")[1]=="jpg" or file.split(".")[1]=="png":#再把图片x5处理后结果保存在目标文件夹
        img=cv2.imread("img/"+file)
        back_names=["_sp.jpg","_gaussian.jpg","_turn_light.jpg","_turn_dark.jpg"] if file.split(".")[1]=="jpg"\
            else ["_sp.png","_gaussian.png","_turn_light.png","_turn_dark.png"]
        all_img=[sp_noise(deepcopy(img)),gaussian_noise(deepcopy(img)),turn_light(deepcopy(img)),turn_dark(deepcopy(img))]
        for i,n in zip(all_img,back_names):
            cv2.imwrite("result/"+file.split(".")[0]+n,i)
    if file.split(".")[1]=="txt":#最后把标签x5处理后结果保存在目标文件夹
        back_names=["_sp.txt","_gaussian.txt","_turn_light.txt","_turn_dark.txt"]
        for i in back_names:
            copyfile("img/"+file, "result/"+file.split(".")[0]+i)
flag=0

if __name__=='__main__':
    files=os.listdir("img")
    console.rule("[bold red]start:数据增强x5", style="green")
    
    pool = mp.Pool(10)
    start_time=time.time()
    pbar = tqdm(total=int(len(files)))
    pbar.set_description('x5')
    update = lambda *args: pbar.update()

    n_proc = 5
    pool = mp.Pool(n_proc)
    res=[]
    for file in files:
        temp=pool.apply_async(doit, (file,), callback=update)
        res.append(temp)
    pool.close()
    pool.join()
    for r in res:
        r.get() # 返回值需要用get()拿到。
    print(round(time.time()-start_time,1))