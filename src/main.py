import os
from rich.progress import track
from shutil import copyfile
import json
import yaml
import time
import multiprocessing as mp
from random import shuffle
from rich.console import Console
from rich.table import Table
import cv2
from copy import deepcopy
from utils import *
console = Console()

class D:
    def __init__(self):
        self.current_dir = os.path.abspath(os.path.join(__file__,"../"))
        self.input_str = ""
        self.flag = True
        self.is_first = True
        self.classes=None
        self.x5_file_len=0
        self.x5_progress=0  
        self.x8_file_len=0
        self.x8_progress=0
        self.progress=None
        self.task=None
        self.start_time=None
        self.cpu_count=mp.cpu_count()
    def start(self):
        try:
            self.current_dir=os.path.abspath(os.path.join(__file__,"../"))
            self.show_msg("info","使用进程个数："+str(self.cpu_count))
            if os.path.exists("config.json"):
                with open("config.json",'r') as f:conf_dict = json.loads(f.read())
                self.classes=conf_dict["names"]
                self.show_msg("info","读取config.json完成,当前类别:"+str(self.classes))
            else:
                self.show_msg("warn","config.json未检测到,不可使用‘s’模块")
            while self.flag:
                self.show_info()
                if self.current_dir:
                    self.show_msg("info", "当前处理文件夹:" + self.current_dir)
                else:
                    self.show_msg("warn", "还未选择处理文件夹,请输入“cd 路径”来选择文件夹")
                self.dress_input(console.input("[bold cyan]输入操作:[not bold][green]"))
        except:self.show_msg("error", "start模块出错")
    def x5_p_process(self,d):#72s（单线程436s），x5后80M
        # print(1)
        # self.progress.update(self.task,advance=1)
        self.x5_progress+=1
        print("  |"+str(round((self.x5_progress/self.x5_file_len)*100,1))+"%:|"+int((self.x5_progress/self.x5_file_len)*73)*"█"+(73-int((self.x5_progress/self.x5_file_len)*73))*"_"+"|",end="\r")
        if self.x5_progress==self.x5_file_len:print("it takes:"+str(round(self.start_time-time.time(),1))+"s|")
    def x8_p_process(self,d):#x5后8s（单线程27.8s），x8后660M
        # print(1)
        # self.progress.update(self.task,advance=1)
        self.x8_progress+=1
        print("  |"+str(round((self.x8_progress/self.x8_file_len)*100,1))+"%:|"+int((self.x8_progress/self.x8_file_len)*73)*"█"+(73-int((self.x8_progress/self.x8_file_len)*73))*"_"+"|",end="\r")
        if self.x8_progress==self.x8_file_len:print("it takes:"+str(round(self.start_time-time.time(),1))+"s|")
    def dress_input(self, input_str):
        try:
            if input_str== "x5":#x5
                if not self.current_dir:self.show_msg("warn", "处理文件夹还未选择，请先选择")
                else:self.x5()
            elif input_str== "x8":#x8
                if not self.current_dir:self.show_msg("warn", "处理文件夹还未选择，请先选择")
                else:self.x8()
            elif input_str== "d":#distort
                if not self.current_dir:self.show_msg("warn", "处理文件夹还未选择，请先选择")
                else:self.distort_dir()
            elif input_str== "m":#make config.json
                self.make_conf()
            elif input_str== "e":#exit
                self.show_msg("info", "期待下次使用本产品！")
                os._exit(0)
            else:#cd，rename
                input_splited = input_str.split(" ")
                if input_splited[0]=="s":#split
                    if not self.current_dir:self.show_msg("warn", "处理文件夹还未选择，请先选择")
                    else:
                        if len(input_splited)==1:self.splite_dir()
                        elif len(input_splited)==2:
                            input_splited_nums=input_splited[1].split(":")
                            if len(input_splited_nums) == 3:self.splite_dir([int(i) for i in input_splited_nums])
                            else:self.show_msg("warn", "比例出错，请注意冒号为英文冒号，数字之和为10，请重新输入")
                        else:self.show_msg("warn", "输入有误，请重新输入")

                elif input_splited[0] == "cd":#cd demo,这一步很关键，确保了self.current_dir一定存在
                    new_path=os.path.join(self.current_dir,input_splited[1])
                    if os.path.exists(new_path):self.current_dir = os.path.abspath(new_path)
                    else:self.show_msg("warn", "处理文件夹不存在或者路径存在空格，请重新输入")

                elif input_splited[0] == "r":#rename
                    if len(input_splited) == 1:
                        if not self.current_dir:self.show_msg("warn", "处理文件夹还未选择，请先选择")
                        else:self.rename()
                    elif len(input_splited) == 2:
                        if not self.current_dir:self.show_msg("warn", "处理文件夹还未选择，请先选择")
                        else:
                            if int(input_splited[1]) < 2 or int(input_splited[1]) > 8:
                                self.show_msg("warn", "输入应:2<=长度<=8,请重新输入")
                            else:self.rename(int(input_splited[1]))
                    else:self.show_msg("warn", "输入有误，请重新输入")
                else:self.show_msg("warn", "输入有误，请重新输入")
        except:self.show_msg("error", "dress_input模块出错")
    def doit_x5(self,file,dest_floder):
        copyfile(self.current_dir+"/"+file, dest_floder+"/"+file)#先把原来所有文件都复制一份
        if file.split(".")[1]=="jpg" or file.split(".")[1]=="png":#再把图片x5处理后结果保存在目标文件夹
            img=cv2.imread(self.current_dir+"/"+file)
            back_names=["_sp.jpg","_gaussian.jpg","_turn_light.jpg","_turn_dark.jpg"] if file.split(".")[1]=="jpg"\
                else ["_sp.png","_gaussian.png","_turn_light.png","_turn_dark.png"]
            all_img=[sp_noise(deepcopy(img)),gaussian_noise(deepcopy(img)),turn_light(deepcopy(img)),turn_dark(deepcopy(img))]
            for i,n in zip(all_img,back_names):
                cv2.imwrite(dest_floder+"/"+file.split(".")[0]+n,i)
        if file.split(".")[1]=="txt":#最后把标签x5处理后结果保存在目标文件夹
            back_names=["_sp.txt","_gaussian.txt","_turn_light.txt","_turn_dark.txt"]
            for i in back_names:
                copyfile(self.current_dir+"/"+file, dest_floder+"/"+file.split(".")[0]+i)
    def doit_x8(self,file,dest_floder):
        if file.split(".")[1]=="jpg" or file.split(".")[1]=="png":
            img=cv2.imread(self.current_dir+"/"+file)
            img2,img3,img4=cv2.flip(img,1),cv2.flip(img,0),cv2.flip(img,-1)
            img_1 = cv2.transpose(img)    # 转置（行列互换）
            img_2,img_3,img_4=cv2.flip(img_1,1),cv2.flip(img_1,0),cv2.flip(img_1,-1)
            all_img=[img,img2,img3,img4,img_1,img_2,img_3,img_4]
            back_names=[".jpg","_f1.jpg","_f0.jpg","_f-1.jpg","_t.jpg","_t_f1.jpg","_t_f0.jpg","_t_f-1.jpg"] \
                if file.split(".")[1]=="jpg"\
                else   [".png","_f1.png","_f0.png","_f-1.png","_t.png","_t_f1.png","_t_f0.png","_t_f-1.png"]        
            for i,n in zip(all_img,back_names):
                cv2.imwrite(dest_floder+"/"+file.split(".")[0]+n,i)
        if file.split(".")[1]=="txt":
            with open(self.current_dir+"/"+file,"r") as f:txt=f.readlines()
            info=[]
            for index,i in enumerate(txt):
                info.append({"class":i.split(" ")[0],"p":[float(i.split(" ")[1]),float(i.split(" ")[2]),float(i.split(" ")[3]),float(i.split(" ")[4][0:-1])]})

            info2=deepcopy(info)
            for i in info2:
                i["p"][0]=1-i["p"][0]
            info3=deepcopy(info)
            for i in info3:
                i["p"][1]=1-i["p"][1]
            info4=deepcopy(info)
            for i in info4:
                i["p"][1]=1-i["p"][1]    
                i["p"][0]=1-i["p"][0]   
            # 旋转90度
            info_1=deepcopy(info)
            for i in info_1:
                temp=i["p"][0]   
                i["p"][0] =i["p"][1]   
                i["p"][1] =temp
                temp=i["p"][2]   
                i["p"][2] =i["p"][3]   
                i["p"][3] =temp  
            info_2=deepcopy(info_1)
            for i in info_2:
                i["p"][0]=1-i["p"][0]
            info_3=deepcopy(info_1)
            for i in info_3:
                i["p"][1]=1-i["p"][1]
            info_4=deepcopy(info_1)
            for i in info_4:
                i["p"][1]=1-i["p"][1]    
                i["p"][0]=1-i["p"][0]   
            all_info=[info,info2,info3,info4,info_1,info_2,info_3,info_4]
            back_names=[".txt","_f1.txt","_f0.txt","_f-1.txt","_t.txt","_t_f1.txt","_t_f0.txt","_t_f-1.txt"]
            for i,n in zip(all_info,back_names):
                str1=""
                for j in i:
                    str1+=j["class"]+" "'%.6f'%j["p"][0]+" "+'%.6f'%j["p"][1]+" "+'%.6f'%j["p"][2]+" "+'%.6f'%j["p"][3]+"\n"
                with open(dest_floder+"/"+file.split(".")[0]+n,"w") as f:
                    f.write(str1)
    def x8(self):
        try:
            dest_floder=self.current_dir+"_x8"
            files=os.listdir(self.current_dir)

            need_del_classes_txt=False
            for file in files:#确保classes.txt这个文件不会被复制到结果中
                if file=="classes.txt":need_del_classes_txt=True
            if need_del_classes_txt:files.remove("classes.txt")

            if not os.path.exists(dest_floder): os.mkdir(dest_floder) 
            self.start_time=time.time()
            console.rule("[bold red]start:数据增强x8", style="green")
            
            self.x8_file_len=len(files)
            pool=mp.Pool(self.cpu_count)
            for file in files:
                pool.apply_async(self.doit_x8, (file,dest_floder,), callback=self.x8_p_process)
            pool.close()
            pool.join()

            console.rule("[bold red]done:数据增强x8", style="green")
            self.x8_file_len=0
            self.x8_progress=0  
        except:self.show_msg("error", "x8模块出错")
    def x5(self):
        try:
            dest_floder=self.current_dir+"_x5"
            if not os.path.exists(dest_floder): os.mkdir(dest_floder) 
            files=os.listdir(self.current_dir)
            need_del_classes_txt=False
            for file in files:#确保classes.txt这个文件不会被复制到结果中
                if file=="classes.txt":need_del_classes_txt=True
            if need_del_classes_txt:files.remove("classes.txt")
            self.start_time=time.time()
            console.rule("[bold red]start:数据增强x5", style="green")

            self.x5_file_len=len(files)
            pool=mp.Pool(self.cpu_count)
            for file in files:
                pool.apply_async(self.doit_x5, (file,dest_floder,), callback=self.x5_p_process)
            pool.close()
            pool.join()

            console.rule("[bold red]done:数据增强x5", style="green")
            self.x5_file_len=0
            self.x5_progress=0    
        except:self.show_msg("error", "x5模块出错")
    def make_conf(self):
        try:
            console.rule("[bold red]start:生成config.json", style="green")
            conf_dict = {"names": {0: "eyes",1: "mouth"}}
            with open("config.json","w") as f:json.dump(conf_dict,f)
            console.rule("[bold red]done:生成config.json", style="green")
        except:self.show_msg("error", "make_conf模块出错")
    def splite_dir(self, ratio=[7,2,1]):
        try:
            if os.path.exists("config.json"):#如果config.json不存在，那么's'划分数据集操作不会被执行
                files = os.listdir(self.current_dir)

                #确保classes.txt这个文件不会被复制到结果中,将files中的classes.txt去除掉
                need_del_classes_txt=False
                for file in files:
                    if file=="classes.txt":need_del_classes_txt=True
                if need_del_classes_txt:files.remove("classes.txt")

                #开始读取config的classes属性
                with open("config.json",'r') as f:conf_dict = json.loads(f.read())
                self.classes=conf_dict["names"]
                self.show_msg("info","读取config.json完成,当前类别:"+str(self.classes)+",开始划分")

                #开始划分
                if ratio[0]+ratio[1]+ratio[2]!=10:self.show_msg("error","比例和不为10，请重新输入")
                else:
                    train = files[0:int(ratio[0] * len(files) / 10)]
                    test = files[int(ratio[0] * len(files) / 10):int((ratio[0] + ratio[1]) * len(files) / 10)]
                    val = files[int((ratio[0] + ratio[1]) * len(files) / 10):int(len(files))]
                    dest_floder_1=self.current_dir + "_s"+str(ratio[0])+str(ratio[1])+str(ratio[2])#dest_floder_1=   xxx_s721
                    
                    console.rule("[bold red]start:数据集划分", style="green")
                    for p1, p2 in zip(["test/", "val/", "train/"], [test, val, train]):#p1文件夹，p2数据集
                        if not os.path.exists(dest_floder_1+"/" + p1 + "images"): os.makedirs(dest_floder_1+"/" + p1 + "images")#创建xxx_s721/test/images
                        if not os.path.exists(dest_floder_1+"/" + p1 + "labels"): os.makedirs(dest_floder_1+"/" + p1 + "labels")#创建xxx_s721/test/labels
                        for i in track(p2,description=p1.split("/")[0]+"_split...:"):
                            dest_floder_2 = "labels/" if i.split(".")[-1] == 'txt' else "images/"#dest_floder_2=   labels/   or labels/
                            copyfile(self.current_dir + "/" + i, dest_floder_1 +"/" + p1 + dest_floder_2 + i)
                    self.show_msg("info", "图片总数量=" + str(int(len(files) / 2)))
                    self.show_msg("info", "train:test:val=" + str(int(len(train) / 2)) + ":" + str(int(len(test) / 2)) + ":" + str(int(len(val) / 2)))  
                    self.show_msg("info","当前类别："+str(self.classes))
                    apiData = {"train": dest_floder_1 +"/train", "test": dest_floder_1 +"/test","val": dest_floder_1 +"/val", "names": self.classes}
                    with open(dest_floder_1 + "/d.yaml", 'w', encoding='utf-8') as f:
                        yaml.dump(data=apiData, stream=f, allow_unicode=True)
                    console.rule("[bold red]done:划分数据集", style="green")
            else:
                self.show_msg("error","未找到config.json,使用'm'命令生成config.json，并设置classes")
        except:self.show_msg("error", "split_dir模块出错")
    def distort_dir(self):
        try:
            files = os.listdir(self.current_dir)
            need_del_classes_txt=False
            for file in files:#确保classes.txt这个文件不会被复制到结果中
                if file=="classes.txt":need_del_classes_txt=True
            if need_del_classes_txt:files.remove("classes.txt")

            l1 = [i for i in range(int(len(files) / 2))]
            shuffle(l1)
            dest_floder = self.current_dir + "_d"
            if not os.path.exists(dest_floder): os.mkdir(dest_floder)
            just_names = []
            for file in files:just_names.append(file.split(".")[0])
            just_names = list(set(just_names))  # 将列表转换为集合再转换为列表（集合没有重复项,合并img和label的名字）
            length = len(l1)
            console.rule("[bold red]start:打乱数据集", style="green")
            for i in track(range(length), description="打乱中  ...:"):
                copyfile(self.current_dir + "/" + just_names[i] + ".txt", dest_floder + "/" + str(i) + ".txt")
                if os.path.exists(self.current_dir + "/" + just_names[i] + ".jpg"):
                    copyfile(self.current_dir + "/" + just_names[i] + ".jpg", dest_floder + "/" + str(i) + ".jpg")
                elif os.path.exists(self.current_dir + "/" + just_names[i] + ".png"):
                    copyfile(self.current_dir + "/" + just_names[i] + ".png", dest_floder + "/" + str(i) + ".png")
            # for name,l in zip(just_names,l1):
            #     copyfile(self.current_dir+"/"+name+".txt",dest_floder+"/"+str(l)+".txt")
            #     if os.path.exists(self.current_dir+"/"+name+".jpg"):
            #         copyfile(self.current_dir+"/"+name+".jpg",dest_floder+"/"+str(l)+".jpg")
            #     else:copyfile(self.current_dir+"/"+name+".png",dest_floder+"/"+str(l)+".png")
            console.rule("[bold red]done:打乱数据集", style="green")
        except:self.show_msg("error", "distort_dir模块出错")
    def rename(self, length=5):
        try:
            files = os.listdir(self.current_dir)
            if length > 8 or length < 2:
                self.show_msg("warn", "未能完成重命名，注意：2<=length<=7")
            else:
                if not len(str(int(len(files) / 2))) <= length:
                    self.show_msg("warn", "未能完成重命名,重命名长度不够，请加大length")
                else:
                    console.rule("[bold red]start:重命名", style="green")
                    new_dir_path = self.current_dir + "_r" + str(length)
                    if not os.path.exists(new_dir_path): os.makedirs(new_dir_path)
                    len_list = ["%02d", "%03d", "%04d", "%05d", "%06d", "%07d", "%08d"]
                    len_num = len_list[length - 2]
                    img_list, label_list = [], []
                    for file in files:
                        if file.split(".")[-1] == "jpg" or file.split(".")[-1] == "png": img_list.append(file)
                        if file.split(".")[-1] == "txt" and file != "classes.txt": label_list.append(file)
                    img_list.sort()
                    label_list.sort()
                    num = 1
                    # 重命名复制img
                    for file in track(img_list, description="重命名img  ...:"):
                        s = len_num % num
                        copyfile(os.path.join(self.current_dir, file),os.path.join(new_dir_path, str(s) + "." + file.split(".")[1]))
                        num += 1
                    num = 1
                    # 重命名复制label
                    for file in track(label_list, description="重命名label...:"):
                        s = len_num % num
                        copyfile(os.path.join(self.current_dir, file),os.path.join(new_dir_path, str(s) + "." + file.split(".")[1]))
                        num += 1
                    console.rule("[bold red]done:重命名", style="green")
        except:self.show_msg("error", "rename模块出错")
    def show_msg(self, cate, txt):
        try:
            if cate == "info": start = "[bold green](info):[not bold][green]"
            if cate == "warn": start = "[bold yellow](warn):[not bold][green]"
            if cate == "error": start = "[bold red](error):[not bold][green]"
            console.print(start + txt)
        except:self.show_msg("error", "show_msg模块出错")
    def show_info(self):
        try:
            if self.is_first:
                table = Table(title="[bold red]yolo数据集处理器云端版(by D-Studio)使用&介绍")
                table.add_column("[green]方法", justify="center", style="green", no_wrap=True)
                table.add_column("[cyan]描述", justify="left", style="cyan")
                table.add_column("[magenta]示例", justify="center", style="magenta")
                table.add_row("cd 路径", "切换当前处理文件夹", "cd D:ddd")
                table.add_row("r 长度", "给img和label按序命名,默认长度为5", "r 5")
                table.add_row("s", "按照数组划分(train:test:val)，默认(7:2:1)", "s 7:2:1")
                table.add_row("x5", "数据集增强1(颜色变换)，包括阴、暗、高斯噪声、椒盐噪声", "x5")
                table.add_row("x8", "数据集增强2(几何变换)，包括翻转旋转等", "x8")
                table.add_row("d", "打乱数据集", "d")
                table.add_row("m", "生成config.json文件", "m")
                table.add_row("e", "退出", "e")
                console.print(table)
                self.is_first = False
            else:
                table = Table(show_header=False)
                table.add_column("[green]方法", justify="center", style="green", no_wrap=True)
                table.add_column("[cyan]描述", justify="left", style="cyan")
                table.add_column("[magenta]示例", justify="center", style="magenta")
                table.add_row("cd 路径", "切换当前处理文件夹", "cd D:ddd")
                table.add_row("r 长度", "给img和label按序命名,默认长度为5", "r 5")
                table.add_row("s", "按照数组划分(train:test:val)，默认(7:2:1)", "s 7:2:1")
                table.add_row("x5", "数据集增强1(颜色变换)，包括阴、暗、高斯噪声、椒盐噪声", "x5")
                table.add_row("x8", "数据集增强2(几何变换)，包括翻转旋转等", "x8")
                table.add_row("d", "打乱数据集", "d")
                table.add_row("m", "生成config.json文件", "m")
                table.add_row("e", "退出", "e")
                console.print(table)
        except:self.show_msg("error", "show_info模块出错")
if __name__=='__main__':
    mp.freeze_support()
    d = D()
    d.start()
